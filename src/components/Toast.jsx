import { useState } from "react";
import { Modal } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";


export const ToastDemo = (props) => {

  const [ showToast, setShow] = useState(props.show);
  const [ message, setMessage] = useState(props.message);

  return (
    <Modal
      size="sm"
      animation={true}
      show={props.show}
      aria-labelledby="example-modal-sizes-title-sm"
      contentClassName={props.background}
    >
      <Modal.Body>
        <FontAwesomeIcon icon={props.icon} /><span> </span>
        {props.message}
      </Modal.Body>
    </Modal>
  )
}