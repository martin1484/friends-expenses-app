import React, { useContext, useState } from 'react';
import { GoogleLogout } from 'react-google-login';
import { useHistory } from 'react-router-dom';
import { AppContext } from '../AppContext';


const {REACT_APP_CLIENT_ID} = process.env;

const Logout = () => {
    const { sessionState } = useContext(AppContext);
    const [ isLogged, setIsLogged] = sessionState;
    const history = useHistory();

    const onSuccess = () => {
        let loggedUser = JSON.parse(localStorage.getItem('loggedUser'));
        console.log('Logout user: ',loggedUser.name);
        localStorage.setItem('loggedUser', null);
        localStorage.setItem('isLogged', false);
        setIsLogged(false);
        history.push('/');
    }

    return (
        <div>
            <GoogleLogout
                clientId={REACT_APP_CLIENT_ID}
                buttonText="Cerrar Sesion"
                onLogoutSuccess={onSuccess}
                style={{marginTop: '100px'}}
            />
        </div>
    )
}

export default Logout;