import React, { useContext } from "react";
import * as ReactBootStrap  from 'react-bootstrap';
import { faHome } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Login from './Login';
import Logout from './Logout';
import { AppContext } from "../AppContext";


const Navbar = () => {
    const { sessionState } = useContext(AppContext);
    const [isLogged, setIsLogged] = sessionState;
    return (
        <React.Fragment>
            <ReactBootStrap.Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <ReactBootStrap.Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <ReactBootStrap.Navbar.Collapse id="responsive-navbar-nav">
                    <ReactBootStrap.Nav className="mr-auto">
                        <ReactBootStrap.Nav.Link href="/"><FontAwesomeIcon icon={faHome}/> Inicio</ReactBootStrap.Nav.Link>
                        <ReactBootStrap.Nav.Link href="/groups">Grupos</ReactBootStrap.Nav.Link>
                    </ReactBootStrap.Nav>
                </ReactBootStrap.Navbar.Collapse>
                {isLogged === true ? <Logout /> : <Login />}
            </ReactBootStrap.Navbar>
        </React.Fragment>
    );
}

export default Navbar;