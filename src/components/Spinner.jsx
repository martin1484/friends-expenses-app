import React from "react";

const Spinner = () => {
  return (
      <div className="spinner-border" style={{marginLeft : "50%", marginTop:"16px", marginBottom:"16px"}} role="status">
        <span className="sr-only">Loading...</span>
      </div>
  );
}

export default Spinner;