import React, { Component, useContext } from 'react';
import { GoogleLogin } from 'react-google-login';
// refresh token
import { refreshTokenSetup } from '../utils/refreshToken';
import { useHistory } from 'react-router-dom';
import { AppContext } from '../AppContext';

const {REACT_APP_CLIENT_ID} = process.env;

const Login = () => {
    const history = useHistory();
    const { sessionState } = useContext(AppContext);
    const [ isLogged, setIsLogged] = sessionState;

    const onSuccess = (res) => {
        console.log('Login success, currentUser: ',res.profileObj);
        refreshTokenSetup(res);
        localStorage.setItem('loggedUser', JSON.stringify(res.profileObj));
        localStorage.setItem('isLogged', true);
        setIsLogged(true);
    }
    
    const onFailure = (res) => {
        console.log('Login failure: ',res);
        localStorage.setItem('loggedUser', null);
        localStorage.setItem('isLogged', false);
    }

    return (
        <div>
            <GoogleLogin
                clientId={REACT_APP_CLIENT_ID}
                buttonText="Iniciar Sesion"
                onSuccess={onSuccess}
                onFailure={onFailure}
                cookiePolicy={'single_host_origin'}
                style={{marginTop: '100px'}}
                isSignedIn={true}
            />
        </div>
    )
}

export default Login;