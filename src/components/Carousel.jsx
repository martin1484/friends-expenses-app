import React, { useContext } from "react";
import { AppContext } from "../AppContext";
import Carousel from 'react-bootstrap/Carousel'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight, faUserFriends, faMoneyCheckAlt } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import Login from './Login';
import * as ReactBootStrap  from 'react-bootstrap';


const BootstrapCarouselDemo = () => {
    const { sessionState } = useContext(AppContext);
    const [isLogged, setIsLogged] = sessionState;

    return (
        <div>
            <div className='container-fluid' >
                <Carousel keyboard={false} pauseOnHover={true}>
                    <Carousel.Item style={{ 'height': "300px" }}  >
                        <img style={{ 'height': "300px" }} className="d-block w-100" src={'assets/img/blue.png'} />
                        <Carousel.Caption>
                            <h3>Hagamos las cuentas!</h3>
                            <p>Esta app te va a ayudar a dividir gastos con amigos.</p>
                            <FontAwesomeIcon icon={faUserFriends} style={{ fontSize: "24px" }} />
                            <FontAwesomeIcon icon={faMoneyCheckAlt} style={{ fontSize: "24px", marginLeft: "16px" }} />
                        </Carousel.Caption>
                    </Carousel.Item  >
                    <Carousel.Item style={{ 'height': "300px" }}>
                        <img style={{ 'height': "300px" }}
                            className="d-block w-100"
                            src={'assets/img/blue.png'} />
                        <Carousel.Caption>
                            <h3>Agregar Grupo</h3>
                            <FontAwesomeIcon icon={faUserFriends} style={{ fontSize: "24px" }} />
                            <p>Aquí agregaras un nuevo grupo con sus integrantes.</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item style={{ 'height': "300px" }}>
                        <img style={{ 'height': "300px" }}
                            className="d-block w-100"
                            src={'assets/img/blue.png'} />
                        <Carousel.Caption>
                            <h3>Agregar gasto</h3>
                            <FontAwesomeIcon icon={faMoneyCheckAlt} style={{ fontSize: "24px" }} />
                            <p>Aquí agregaras un gasto nuevo.</p>
                            <div className="btnRight col-sm-12 col-md-12 col-lg-12" style={{ marginTop: "24px" }}>
                                {isLogged === true ?
                                    <ReactBootStrap.Nav.Link href="/groups">
                                        <button className="btn btn-success btn-sm">
                                            <span>Empezar </span>
                                            <FontAwesomeIcon icon={faArrowRight} />
                                        </button>
                                    </ReactBootStrap.Nav.Link>
                                    :
                                    <Login />
                                }

                            </div>
                        </Carousel.Caption>
                    </Carousel.Item>
                </Carousel>
            </div>
        </div>
    );
}


export default BootstrapCarouselDemo;