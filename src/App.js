
import "./App.css"
import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import Navbar from './components/Navbar'
import MainPage from "./pages";
import NotFoundPage from "./pages/404";
import GroupsPage from "./pages/groups";
import FormGroup from "./pages/formGroup";
import FormGroupEdit from "./pages/formGroupEdit";
import GuardedRoute from "./components/GuardRoute";
import ExpensesPage from "./pages/expenses";
import GroupsDetails from "./pages/groupDetails";
import MemberDetails from "./pages/memberDetails";
import { AppProvider } from "./AppContext";
import ExpensesEdit from "./pages/expensesEdit";
import DivideExpenses from "./pages/divideExpeses";


class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      isLogged : localStorage.getItem('isLogged')
    }
  }
  
  render() {
    return (
      <AppProvider>
        <Router>
          <Navbar/>
          <Switch>
            <Route exact path="/" component={MainPage} />
            <GuardedRoute exact path="/groups" component={GroupsPage} auth={this.state.isLogged} />
            <Route exact path="/404" component={NotFoundPage} />
            <GuardedRoute exact path="/formGroup" component={FormGroup} auth={this.state.isLogged} />
            <GuardedRoute exact path="/formGroupEdit" component={FormGroupEdit} auth={this.state.isLogged} />
            <GuardedRoute exact path="/expenses" component={ExpensesPage} auth={this.state.isLogged} />
            <GuardedRoute exact path="/groupDetails/:id" component={GroupsDetails} auth={this.state.isLogged} />
            <GuardedRoute exact path="/memberDetails" component={MemberDetails} auth={this.state.isLogged} />
            <GuardedRoute exact path="/expensesEdit/:id" component={ExpensesEdit} auth={this.state.isLogged} />
            <GuardedRoute exact path="/divideExpenses/:id" component={DivideExpenses} auth={this.state.isLogged} />
            <Redirect to="/404"/>
          </Switch>
        </Router>
      </AppProvider>
    )

  }
    
}

export default App;
