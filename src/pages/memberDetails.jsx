import React, {Component} from "react";
import { Link } from "react-router-dom";
import style from "./style.css";
import Spinner from '../components/Spinner'
import { formatDate, currencyFormatter, elipsisData } from '../utils/CommonUtilities';
import { faPlus, faExclamationCircle, faCheckCircle, faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Modal } from "react-bootstrap";
import { Button} from 'react-bootstrap';

class MemberDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
          group: {},
          membersList : [],
          loggedUser : JSON.parse(localStorage.getItem('loggedUser')),
          loader: true,
          globalHistory: props.history,
          member : props.location.state.member,
          group : props.location.state.group,
          showDialog : false,
          dialogMessage : "",
          dialogStyle : "",
          expenseDetailMemberMessage : 'Detalle de compra',
          dialogExpenseEdit : null,
        }
    }

    componentDidMount() {
        
    }

    back = () => {
        let pathReturn =  '/groupDetails/'+this.state.group.id;
        this.state.globalHistory.push({
            pathname: pathReturn
         })
    }

    onClickExpenseDetail = (expense) => {
        this.showExpenseDetailDialog(expense);
    }

    showExpenseDetailDialog = (expense) => {
        this.setState({
          showDialog: true,
          dialogMessageDescription: expense.description,
          dialogMessageDate: formatDate(expense.creationDate),
          dialogMessageAmount: currencyFormatter.format(expense.amount),
          dialogStyle: "modal-warning-member-delete",
          dialogExpenseEdit: expense
        });
      }
      
      closeDialog() {
        this.setState({
          showDialog : false
        })
      }

      editExpense() {
        let pathReturn =  '/expensesEdit/'+this.state.dialogExpenseEdit.id;
        this.state.globalHistory.push({
            pathname: pathReturn,
            expense: this.state.dialogExpenseEdit,
            group: this.state.group
         })
      }

      adminPermissions = () => {
        var arrAdmins = this.state.group.admins.filter(this.compareEmails);
          if(this.state.member.email == this.state.loggedUser.email || arrAdmins.length > 0){ //si el usuario logueado es admin o es el dueno del gasto puede editarlo
              return true
          }
    }

    compareEmails = (elemento) => {
        if (elemento.email == this.state.loggedUser.email){
          return elemento;
        }
      }

    render() {
        return (
            <React.Fragment>
                <div className="container">
                    <div className="jumbotron">
                    <div className = "cursorPointer" style = {{marginBottom:"24px"}}onClick={() => this.back()}>
                        <FontAwesomeIcon icon={faArrowLeft} />
                    </div>

                    <Modal
                        size="sm"
                        animation={true}
                        show={this.state.showDialog}
                        aria-labelledby="example-modal-sizes-title-sm"
                        contentClassName={this.state.dialogStyle}
                        onHide={() => this.setState({
                            showDialog : false
                        })}
                        aria-labelledby="example-modal-sizes-title-sm"
                        >
                        
                        <Modal.Header closeButton>
                            <Modal.Title>{this.state.expenseDetailMemberMessage}</Modal.Title>
                        </Modal.Header>

                        <Modal.Body>
                            <p><b>Fecha:</b> {this.state.dialogMessageDate}</p>
                            <p><b>Descripción:</b> {this.state.dialogMessageDescription}</p>
                            <p><b>Monto:</b> {this.state.dialogMessageAmount}</p>
                        </Modal.Body>

                        <Modal.Footer>
                            <Button variant="secondary"onClick={() => this.closeDialog()}>Cerrar</Button>
                            {   this.adminPermissions() &&
                                <Button variant="info" onClick={() => this.editExpense()}>Editar</Button>
                            }
                        </Modal.Footer>
                    </Modal>


                        <div className="card bg-light mb-3" >
                            <div class="card-header">
                                Información de Miembro
                            </div>
                                <div class="card-body">
                                    <h5 class="card-title">{this.state.member.firstName+" "+this.state.member.lastName}</h5>
                                    <label className="card-text">Total gastado: <spam className="font-weight-bold text-primary">{currencyFormatter.format(this.state.member.memberTotalAmount)}</spam></label>
                                    <br/>
                                    { (this.state.member.memberDifferenceAmount >= 0) &&
                                    <label className="card-text">Saldo a favor: <spam className="font-weight-bold text-success">{currencyFormatter.format(this.state.member.memberDifferenceAmount)}</spam></label>
                                    }
                                    { (this.state.member.memberDifferenceAmount < 0) &&
                                    <label className="card-text">Saldo en contra: <spam className="font-weight-bold text-danger">{currencyFormatter.format(this.state.member.memberDifferenceAmount)}</spam></label>
                                    }    
                                </div>   
                        </div>
                        <div className="card bg-light mb-3" >
                            <div class="card-header">
                                Gastos
                            </div>
                            
                            <div class="card-body">
                            { (this.state.member.expenses.length > 0) &&
                                <div className="table-responsive">
                                    <table className="table table-md">
                                        <thead>
                                            <tr>
                                                <th>Fecha gasto</th>
                                                <th>Detalle</th>
                                                <th>Monto</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {this.state.member.expenses.map((expense) => (
                                                <tr className="cursorPointer" key={expense.id} onClick={() => this.onClickExpenseDetail(expense)}>
                                                    <td>{formatDate(expense.creationDate, "short")}</td>
                                                    <td>{expense.description.length > 15 ? elipsisData(expense.description, 15) : expense.description}  </td>
                                                    <td>{currencyFormatter.format(expense.amount)}</td>
                                                </tr>
                                        ))}
                                        </tbody>
                                    </table>
                                </div>
                                 }
                            </div>
                          
                            { !(this.state.member.expenses.length > 0) &&
                           <div className="container" style={{marginBottom: "16px"}}>
                                <label className="font-weight-bold text-info">{this.state.member.firstName+" "+this.state.member.lastName} no cuenta con ningún gasto</label>
                           </div>
                            }
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default MemberDetails;