import Carousel from 'react-bootstrap/Carousel'
import React, {Component} from "react";
import { Link } from "react-router-dom";
import BootstrapCarouselDemo from '../components/Carousel'
   


    class MainPage extends Component {

        constructor(props) {
            super(props);
            this.state = {
              loggedUser : JSON.parse(localStorage.getItem('loggedUser')),
              globalHistory: props.history,
              showStep1 : false,
              showStep2 : false,
              showStep3 : false,
            }
        }

        showStep1 = () => {
            this.setState({
                showStep1 : true,
                showStep2 : false,
                showStep3 : false
              })
        }

        showStep2 = () => {
            this.setState({
                showStep1 : false,
                showStep2 : true,
                showStep3 : false
              })
        }

        showStep3 = () => {
            this.setState({
                showStep1 : false,
                showStep2 : false,
                showStep3 : true
              })
        }

         showGroupList= () =>{
                this.state.globalHistory.push({
                pathname: '/groups',
            });
        }



        render() {
            return (
                <React.Fragment>
                    <div className="container col-sm-12 col-lg-6">
                        <div className="jumbotron">
                            <BootstrapCarouselDemo />
                        </div>
                    </div>
                </React.Fragment>
            )
        }
}

export default MainPage;