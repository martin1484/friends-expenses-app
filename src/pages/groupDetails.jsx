import React, {Component} from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit } from "@fortawesome/free-solid-svg-icons";
import Spinner from '../components/Spinner'
import { formatDate, currencyFormatter } from '../utils/CommonUtilities';
import { faPlus, faArrowLeft, faMoneyCheckAlt, faCalculator } from "@fortawesome/free-solid-svg-icons";

const {REACT_APP_BASE_URL_API} = process.env;

class GroupsDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
          group: {},
          membersList : [],
          loggedUser : JSON.parse(localStorage.getItem('loggedUser')),
          groupId: props.match.params.id,
          loader: true,
          globalHistory: props.history,
        }
        this.showMemberDetail.bind(this);
        

    }

    componentDidMount() {
         const url = `${REACT_APP_BASE_URL_API}/group/${this.state.groupId}`;
         fetch(url)
          .then(respuesta => respuesta.json())
          .then(resultado => this.setState({
                group: resultado,
                membersList: resultado.members,
                loader : false
            }))
            .catch(e => {
                console.log(e)
                this.setState({
                    loader: false
                })
            })
      }

      showMemberDetail= (member) =>{
         this.state.globalHistory.push({
            pathname: '/memberDetails',
            state: {
                member: member,
                group: this.state.group
            }
         });
      }

      showEditForm= (group) =>{
        this.state.globalHistory.push({
           pathname: '/formGroupEdit',
           state: {
                group: group
           }
        });
     }

     showExpenseForm= (group) =>{
        this.state.globalHistory.push({
           pathname: '/expenses',
           state: {
                group: group
           }
        });
     }

     back = () => {
        let pathReturn =  '/groups';
        this.state.globalHistory.push({
            pathname: pathReturn
         })
    }

    showDivideExpenses = (group) => {
        this.state.globalHistory.push({
            pathname: '/divideExpenses/'+ this.state.group.id,
            state: {
                 group: group
            }
         });
    }

    adminPermissions = () => {
        var arrAdmins = this.state.group.admins.filter(this.compareEmails);
        return arrAdmins.length > 0;
    }

      compareEmails = (elemento) => {
        if (elemento.email == this.state.loggedUser.email){
          return elemento;
        }
      }

   

    render() {
        return (
            <React.Fragment>
                <div className="container">
                    <div className="jumbotron">
                    <div className = "cursorPointer" style = {{marginBottom:"24px"}}onClick={() => this.back()}>
                        <FontAwesomeIcon icon={faArrowLeft} />
                    </div>
                        <div className="card bg-light mb-3" >
                            <div class="card-header" style={{display:"flex", justifyContent: "space-between"}}>
                            <div>
                                <h5 class="card-title">{this.state.group.name}</h5>
                            </div>
                                <div>         
                                    <button className="btn btn-success btn-md" onClick={() => this.showExpenseForm(this.state.group)}>
                                        <FontAwesomeIcon icon={faMoneyCheckAlt} />
                                        <span  className="textToHide"> Agregar gasto</span>
                                    </button>
                                </div>
                            </div>
                            { !this.state.loader &&
                                <div class="card-body">
                                    <div style={{display: "flex", justifyContent: "space-between"}}>
                                        <div>
                                            <label className="card-text">Total gastado: <spam className="font-weight-bold text-info">{currencyFormatter.format(this.state.group.groupTotalAmount)}</spam></label>
                                        </div>
                                    { this.adminPermissions() &&
                                        <div>       
                                            <button className="btn btn-info btn-md" onClick={() => this.showEditForm(this.state.group)}>
                                                <FontAwesomeIcon icon={faEdit} />
                                                <span className="textToHide"> Editar grupo</span>
                                            </button>
                                        </div>
                                    }
                                    </div>
                                    <label  className="card-text">Fecha de creación: {formatDate(this.state.group.creationDate)}</label>
                                    <br/>
                                    <div style={{display: "flex", justifyContent: "flex-end", alignContent: "space-between",flexDirection: "column"}}>
                                        <div><label className="card-text">Total gastado por integrante:</label></div>
                                        <div><label className="descriptionLabel"> (Total/Cant. Integrantes)</label></div>
                                        <div><spam className="font-weight-bold text-primary">{currencyFormatter.format(this.state.group.amountPerMember)}</spam></div>
                                    </div>
                    
                                    
                                </div>
                            }
                            { this.state.loader &&
                                <Spinner/>
                            }
                            
                        </div>
                        <div className="card bg-light mb-3" >
                            <div class="card-header">
                                Integrantes
                            </div>
                            { !this.state.loader &&
                            <div class="card-body">
                                <div className="table-responsive">
                                    <table className="table table-md">
                                        <thead>
                                            <tr>
                                                <th>Nombre de Integrante</th>
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.membersList.map((member) => (
                                                <tr className="cursorPointer" key={member.id} onClick={() => this.showMemberDetail(member)}>
                                                    <td>{member.firstName +" "+ member.lastName}</td>
                                                    <td>{currencyFormatter.format(member.memberTotalAmount)}</td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            }
                            { this.state.loader &&
                                <Spinner/>
                            }
                        </div>
                        { this.state.group.groupTotalAmount > 0 &&
                        <div  style={{display:"flex",flexDirection: "row" ,justifyContent: "flex-end"}}>
                            <div >
                                <button className="btn btn-primary btn-md" onClick={() => this.showDivideExpenses(this.state.group)}>
                                    <FontAwesomeIcon icon={faCalculator} />
                                    <span> Hacer las cuentas</span>
                                </button>
                            </div>
                        </div>
                        }
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default GroupsDetails;