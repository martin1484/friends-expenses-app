import * as ReactBootStrap  from 'react-bootstrap';
import React, { Component } from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { faCalendar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {ToastDemo} from '../components/Toast';
import { faPlus, faExclamationCircle, faCheckCircle, faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import Spinner from '../components/Spinner';

const {REACT_APP_BASE_URL_API} = process.env;



class ExpensesEdit extends Component {

    description = React.createRef();
    amount = React.createRef();
    API_ENDPOINT = `${REACT_APP_BASE_URL_API}/expense`;

    constructor(props) {
        super(props);
        this.state = {
            groupList : [],
            groupId : null,
            descriptionError : false,
            amountError : false,
            loggedUser : JSON.parse(localStorage.getItem('loggedUser')),
            creationDate : new Date(),
            globalHistory: props.history,
            showToast : false,
            toastMessage : "",
            toastStyle : "",
            toastIcon : "",
            loader : false,
            expense : props.location.expense,
            group : props.location.group,
        }
    }

    componentDidMount() {
        const url = `${REACT_APP_BASE_URL_API}/group/search?email=${this.state.loggedUser.email}`;
        fetch(url)
          .then(respuesta => respuesta.json())
          .then(resultado => this.setState({
                groupList :resultado
            }))
            .catch(e => {
                console.log(e)
            })
            this.loadExpenseInfo(this.state.expense);
      }

      loadExpenseInfo = (expense) => {
        this.description.current.value = expense.description;
        this.amount.current.value = expense.amount;
        this.setState({
            creationDate : new Date(expense.creationDate)
        })
      }
    
    onSelect = (e) => {
        var group = JSON.parse(e.target.value);
        this.setState({
            groupId :group.id
          })
    }

    onSubmit = (e) =>{
        e.preventDefault();
        let pathReturn =  '/groupDetails/'+this.state.group.id;
        var creationDate = this.generateDate(this.state.creationDate);
        let expense = {
            groupId: this.state.group.id,
            createdBy: this.state.loggedUser.email,
            description: this.description.current.value.trim(),
            amount: this.amount.current.value.trim(),
            creationDate: creationDate
        }
        
        if(this.validateSubmit(expense)){
            this.updateExpense(expense);
            this.description.current.value = "";
            this.amount.current.value = "";
            this.setState({
                creationDate: new Date,
                loader : true
            });
            var message = "El gasto se editó correctamente";
            this.showSuccessToast(message, pathReturn);
        } else {
            var message = "El gasto no pudo ser guardado";
            this.showErrorToast(message);
        }
        
    }

    validateSubmit = (expense) => {
        var validGroupId = true;
        var validDescription = true;
        var validAmount = true;
        var validForm = false;
        if(expense.groupId === ""){
            validGroupId = false; //Si hay error, el formulario no es valido
        } else {
            validGroupId = true; //Si no hay error, el formulario es valido
        }
        if(expense.description === ""){
            this.setState({ descriptionError : true });
            validDescription = false; //Si hay error, el formulario no es valido
        } else {
            this.setState ({ descriptionError : false })
            validDescription = true; //Si no hay error, el formulario es valido
        }
        if(expense.amount === ""){
            this.setState({ amountError: true });
            validAmount = false; //Si hay error, el formulario no es valido
        } else {
            this.setState({ amountError: false });
            validAmount = true; //Si no hay error, el formulario es valido
        }
        if(validGroupId && validDescription && validAmount){
            validForm = true;
        }

        return validForm;
    }

    updateExpense = (expense) => {
        var API_ENDPOINT = this.API_ENDPOINT + "/" + this.state.expense.id
        fetch(API_ENDPOINT , {
          "method": "PUT",
          "headers": {
            "content-type": "application/json",
            "accept": "application/json"
          },
          "body": JSON.stringify(expense)
        })
          .then(respuesta => respuesta.json())
          .then(resultado => {
            console.log(resultado)
          })
          .catch(e => {
            console.log(e)
          })
    
      }

    generateDate = (timeStamp) => {
        return timeStamp.toISOString().slice(0,10);
    }

    showSuccessToast = (message, pathReturn) => {
        this.setState({
          showToast: true,
          toastMessage: message,
          toastStyle: "modal-create-group-success",
          toastIcon : faCheckCircle
        });
        setTimeout(function() { //Start the timer
          this.setState({showToast : false}) //After 1 second, set showModal to false
          this.state.globalHistory.push({
            pathname: pathReturn
         })
        }.bind(this), 2000)
    }

    back = () => {
        let pathReturn =  '/groupDetails/'+this.state.group.id;
        this.state.globalHistory.push({
            pathname: pathReturn
         })
    }

    showErrorToast = (message) => {
        this.setState({
          showToast: true,
          toastMessage: message,
          toastStyle: "modal-create-group-error",
          toastIcon : faExclamationCircle
        });
        setTimeout(function() { //Start the timer
          this.setState({showToast : false}) //After 1 second, set showModal to false
        }.bind(this), 2000)
      }


    render() {
        return (
            <React.Fragment>
                 <ToastDemo show={this.state.showToast} message={this.state.toastMessage} background={this.state.toastStyle} icon={this.state.toastIcon}/> <ToastDemo show={this.state.showToast} message={this.state.toastMessage} background={this.state.toastStyle} icon={this.state.toastIcon}/>
                <div className="container col-sm-12 col-lg-4">
                { !this.state.loader && 
                    <div className="jumbotron">
                    <div className = "cursorPointer" onClick={() => this.back()}>
                        <FontAwesomeIcon icon={faArrowLeft} /><span> </span>
                        <label className = "offset-sm-0 offset-sm-1 offset-sm-1 titlePage">{this.state.group.name}</label>
                    </div>
                        <form onSubmit={this.onSubmit}>
                            <div className="col-sm-12 offset-sm-0 col-md-8 offset-md-1 col-lg-8 offset-lg-1" style={{ marginTop: "16px", width: "100%" }}>
                                <label style={{fontSize:"24px"}}>Editar gasto</label>
                            </div>
                            <div style={{ marginTop: "16px", marginBottom: "24px", width: "100%" }}>
                                <div className="col-sm-12 offset-sm-0 col-md-8 offset-md-1 col-lg-8 offset-lg-1">
                                    <label className="form-label" style={{ marginTop: "16px" }}>Descripción<span style={{color: "red"}}> * </span></label>
                                    <input ref={this.description} type="text" className="form-control" id="email" style={{marginBottom: "1rem"}} placeholder="" />
                                    { this.state.descriptionError &&
                                    <div>
                                        <label className="errorLabel">
                                            *Descripción requerido.
                                        </label>
                                    </div>
                                    }
                                    <label className="form-label" style={{ marginTop: "16px" }}>Monto<span style={{color: "red"}}> * </span></label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">$</span>
                                        </div>
                                        <input ref={this.amount} type="number" step="any" class="form-control" aria-label="Amount"></input>
                                    </div>
                                    { this.state.amountError &&
                                    <label className="errorLabel">
                                        *Monto requerido
                                    </label>
                                    }
                                    <div style={{ marginTop: "16px" }}>
                                        <label className="form-label" style={{ marginTop: "16px" }}>Fecha<span style={{color: "red"}}> * </span></label>
                                        <div class="input-group mb-3">
                                            <span class="input-group-text"><FontAwesomeIcon icon={faCalendar} /></span>
                                            <DatePicker dateFormat="dd/MM/yyyy" selected={this.state.creationDate}  onChange={date => this.setState({creationDate: date})} onKeyDown={e => e.preventDefault()} />
                                        </div>
                                    </div>
                                    <div>
                                        <div className="col-sm-12 col-md-12 col-lg-12 btnsContainer"style={{display: "flex", justifyContent: "space-between"}}>
                                            <button className="btn btn-success btn-md buttonSize" type="submit"
                                            >Aceptar</button>
                                            <button className="btn btn-outline-secondary btn-md buttonSize" style={{ marginLeft: "24px" }} onClick={() => this.back()}>Cancelar</button>
                                        </div>
                                    </div>
                                    
                                </div>

                                
                            </div>
                        </form>
                    </div>
                }
                { this.state.loader &&
                    <div>
                        <Spinner/>
                    </div>
                }
                </div>

            </React.Fragment>
        )
    }
}
export default ExpensesEdit;