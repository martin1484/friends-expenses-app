import React, {Component} from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit } from "@fortawesome/free-solid-svg-icons";
import Spinner from '../components/Spinner'
import { formatDate, currencyFormatter } from '../utils/CommonUtilities';
import { faPlus, faArrowLeft, faMoneyCheckAlt } from "@fortawesome/free-solid-svg-icons";

const {REACT_APP_BASE_URL_API} = process.env;

class DivideExpenses extends Component {

    constructor(props) {
        super(props);
        this.state = {
          group: props.location.state.group,
          arrResultDivideExpenses : [],
          loggedUser : JSON.parse(localStorage.getItem('loggedUser')),
          groupId: props.match.params.id,
          loader: true,
          globalHistory: props.history,
        }
        this.showMemberDetail.bind(this);
        

    }

    componentDidMount() {
         const url = `${REACT_APP_BASE_URL_API}/group/calculate/${this.state.groupId}`;
         fetch(url)
          .then(respuesta => respuesta.json())
          .then(resultado => this.setState({
                arrResultDivideExpenses: resultado,
                loader : false
            }))
            .catch(e => {
                console.log(e)
            })
      }

      showMemberDetail= (member) =>{
         this.state.globalHistory.push({
            pathname: '/memberDetails',
            state: {
                member: member,
                group: this.state.group
            }
         });
      }

      showEditForm= (group) =>{
        this.state.globalHistory.push({
           pathname: '/formGroupEdit',
           state: {
                group: group
           }
        });
     }

     showExpenseForm= (group) =>{
        this.state.globalHistory.push({
           pathname: '/expenses',
           state: {
                group: group
           }
        });
     }

     back = () => {
        let pathReturn =  '/groupDetails/' + this.state.groupId;
        this.state.globalHistory.push({
            pathname: pathReturn
         })
    }

    render() {
        return (
            <React.Fragment>
                <div className="container">
                    <div className="jumbotron">
                    <div className = "cursorPointer" style = {{marginBottom:"24px"}}onClick={() => this.back()}>
                        <FontAwesomeIcon icon={faArrowLeft} />
                    </div>
                        { !this.state.loader &&
                            <div style={{display: "flex", justifyContent: "space-between", alignContent: "center", flexWrap: "wrap", flexDirection: "row", paddingBottom:"16px"}}>
                                <h5>{this.state.group.name}</h5>
                                <label className="card-text">Total gastado: <spam className="font-weight-bold text-info">{currencyFormatter.format(this.state.group.groupTotalAmount)}</spam></label>     
                            </div>
                        
                        }
                        <div>

                        { !this.state.loader &&

                                <div className="card">
                                                    
                                    <div  >
                                        {this.state.arrResultDivideExpenses.map((resultItem) => (
                                        <div   key={resultItem.sourceMember.id} style={{borderBottom :"1px solid #ccc"}}>
                                            <div className= "card-header">
                                                <td>{resultItem.sourceMember.firstName +" "+ resultItem.sourceMember.lastName} <span style={{fontWeight:"bold", fontSize:"18px"}}>debe</span>  <spam className="font-weight-bold text-danger">{currencyFormatter.format(Math.abs(resultItem.sourceMember.memberDifferenceAmount))}</spam></td>
                                            </div>
                                            <div>
                                            <div className="card-body">
                                                <td><span style={{fontSize:"18px"}}>Le paga a : </span> </td>
                                            </div>
                                            {resultItem.destinationMembers.map((destinationItem) => (
                                                <div key={destinationItem.destinationMember.id} className="paddingLeftTop">
                                                    <td> {destinationItem.destinationMember.firstName +" "+ destinationItem.destinationMember.lastName} <spam className="font-weight-bold text-success">{currencyFormatter.format(destinationItem.amount)}</spam></td>
                                                </div>
                                                  ))}
                                            </div>
                                        </div>
                                        ))}
                                    </div>
                                                    
                                </div>
                           
                            }
                           
                            { this.state.loader &&
                                <Spinner/>
                            }
                        </div>
                        
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default DivideExpenses;