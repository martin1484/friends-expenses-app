import React, {Component} from "react";
import { Link } from "react-router-dom";
import style from "./style.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch, faPlus, faUserFriends } from "@fortawesome/free-solid-svg-icons";
import Spinner from '../components/Spinner'

const {REACT_APP_BASE_URL_API} = process.env;

class GroupsPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            groupList : [],
            loggedUser : JSON.parse(localStorage.getItem('loggedUser')),
            showGroupsTable : false,
            loader : true,
            globalHistory: props.history
        }
    }

    componentDidMount() {
        const url = `${REACT_APP_BASE_URL_API}/group/search?email=${this.state.loggedUser.email}`;
        fetch(url)
          .then(respuesta => respuesta.json())
          .then(resultado => this.setState({
              groupList :resultado,
              loader : false
            }))
            .catch(e => {
                console.log(e)
                this.setState({
                    loader: false
                })
            })
      }

      showGroupDetail= (groupId) =>{
        this.state.globalHistory.push({
           pathname: '/groupDetails'+ "/" + groupId,
        });
     }

    render() {
        return (
            <React.Fragment>
                <div className="container">
                    <div className="jumbotron">
                        <div style={{ marginBottom: "16px" }}>
                            <label className="titleClass">
                                Grupos
                            </label>
                            {!this.state.loader &&
                            <div className="btnRight">
                                <Link to="/formGroup">
                                    <button className="btn btn-primary btn-md">
                                        <FontAwesomeIcon icon={faUserFriends} />
                                        <span className="textToHide"> Nuevo grupo</span>
                                    </button>
                                </Link>
                            </div>
                            }     
                        </div>
                          
                        { (this.state.groupList.length > 0 && !this.state.loader)  &&
                        <div className="table-responsive">
                            <table className="table table-md">
                                <thead>
                                    <tr>
                                        <th>Nombre de grupo</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {this.state.groupList.map((group) => (
                                    <tr className="cursorPointer" key={group.id} onClick={() => this.showGroupDetail(group.id)}>
                                        <td>{group.name}</td>
                                    </tr>
                                 ))}
                                </tbody>
                            </table>
                        </div>
                        }
                        { (this.state.groupList.length < 1 && !this.state.loader) &&
                        <div>
                            <label className="font-weight-bold text-info">No cuentas con ningún grupo</label>
                        </div>
                        }
                        { this.state.loader &&
                        <div>
                             <Spinner/>
                        </div>
                        }
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default GroupsPage;