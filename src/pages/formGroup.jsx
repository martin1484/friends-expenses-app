import { Link, Redirect, useHistory  } from "react-router-dom";
import { faPlus, faExclamationCircle, faCheckCircle, faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { Component } from 'react';
import { faTrash, faUserPlus } from "@fortawesome/free-solid-svg-icons";
import { Modal } from "react-bootstrap";
import {ToastDemo} from '../components/Toast'
import { getQueriesForElement } from "@testing-library/dom";
import Spinner from '../components/Spinner'
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css'
import { Button} from 'react-bootstrap';



const {REACT_APP_BASE_URL_API} = process.env;

class FormGroup extends Component {
  groupName = React.createRef();
  memberName = React.createRef();
  memberLastname = React.createRef();
  memberEmail = React.createRef();
  API_ENDPOINT = `${REACT_APP_BASE_URL_API}/group`;
  

  constructor(props) {
    super(props);
    this.state = {
      showMemberForm: false,
      showBtn: true,
      membersList: [],
      adminList: [],
      showMembersTable: false,
      disabledBtn: true,
      disabledBtnAddMember: true,
      errorEmail: false,
      emailErrorMsg: "",
      errorValidateEmail: "*Por favor ingrese un email válido",
      errorRequieredEmail: "*Email requerido.",
      errorMemberName: false,
      errorMemberLastName: false,
      loggedUser : JSON.parse(localStorage.getItem('loggedUser')),
      showToast : false,
      toastMessage : "",
      toastStyle : "",
      toastIcon : "",
      globalHistory: props.history,
      loader : false,
      memberDeleteSuccessMessage : "Se eliminó correctamente el miembro",
      memberDeleteErrorMessage : "Debe haber al menos un miembro",
      showDialog : false,
      dialogMessage : "",
      dialogStyle : "",
      warningMemberMessage : 'Si se elimina, usted no podrá ver éste grupo, ¿seguro que desea borrarse?',
      dialogMemberDelete : null
    }
    this.addMember = this.addMember.bind(this);
    this.toggleMemberForm = this.toggleMemberForm.bind(this);
    this.submitForm = this.submitForm.bind(this);
    this.onChangeGroupName = this.onChangeGroupName.bind(this);
    this.validateEmail = this.validateEmail.bind(this);

  }
  
  componentDidMount() {
    this.addMeOnGroup();
  }

  toggleMemberForm = (memberFormVisible) => {
    this.setState({
      showMemberForm: memberFormVisible,
      showBtn: !memberFormVisible,
      errorMemberLastName: false,
      errorMemberName: false,
      errorEmail: false,
    })
  }

  submitForm = (e) => {
    e.preventDefault();
    let group = {
      name: this.groupName.current.value,
      members: this.state.membersList,
      admins: this.state.adminList
    }
    console.log(this.groupName.current.value);

    this.createGroup(group);
    var message = "El grupo se creo correctamente";
    var showGroupList = true;
    this.showSuccessToast(message, showGroupList);
    this.toggleMemberForm(false);
    this.setState({
      disabledBtn: true,
      membersList: [],
      adminList: [],
      showMembersTable: false,
      loader : true
    });
    this.groupName.current.value = "";
  }

  createGroup = (group) => {

    fetch(this.API_ENDPOINT, {
      "method": "POST",
      "headers": {
        "content-type": "application/json",
        "accept": "application/json"
      },
      "body": JSON.stringify(group)
    })
      .then(respuesta => respuesta.json())
      .then(resultado => {
        console.log(resultado)
      })
      .catch(e => {
        console.log(e)
      })

  }

  addMember = (e) => {
    e.preventDefault();
    var errorName = false;
    var errorLastName = false;
    let member = {
      firstName: this.memberName.current.value.trim(),
      lastName: this.memberLastname.current.value.trim(),
      email: this.memberEmail.current.value.trim(),
    }
    if (member.name === "") {
      errorName = true;
    }
    if (member.lastName === "") {
      errorLastName = true;
    } 
    if (member.email === "") {
      this.setState({
        errorEmail: true,
        emailErrorMsg: this.state.errorRequieredEmail
      })
    }
    if (!this.state.errorEmail && !errorName && !errorLastName && !this.alreadyExistsMember(member)) {
      if (this.memberEmail.current.value) {
        this.memberName.current.value = "";
        this.memberLastname.current.value = "";
        this.memberEmail.current.value = "";
        const newMembers = [...this.state.membersList, member]
        this.setState({
          membersList: newMembers,
          disabledBtnAddMember: true,
          disabledBtn: false,
          showMembersTable : true,
        });
        this.toggleMemberForm(false);
      }
      var message = "Miembro agregado correctamente";
      this.showSuccessToast(message);
    } else {
      var message = "El email del miembro ya existe";
      this.showErrorToast(message);
    }
  }

  onChangeGroupName = (e) => {
    e.preventDefault();
    if (this.groupName.current.value.trim() == "" || this.state.membersList.length < 1) {
      this.setState({
        disabledBtn: true
      });
    } else {
      this.setState({
        disabledBtn: false
      });
    }

  }

  validateEmail = (e) => {
    e.preventDefault();
    this.validateDisabledBtnAddMember();
    if (this.memberEmail.current.value) {
      let posicionArroba = this.memberEmail.current.value.lastIndexOf('@');
      let posicionPunto = this.memberEmail.current.value.lastIndexOf('.');

      if (!(posicionArroba < posicionPunto && posicionArroba > 0 && this.memberEmail.current.value.indexOf('@@') == -1 && posicionPunto > 2 && (this.memberEmail.current.value.length - posicionPunto) > 2)) {
        this.setState({
          errorEmail: true,
          emailErrorMsg: this.state.errorValidateEmail,
          disabledBtnAddMember: true
        });
      } else {
        this.setState({
          errorEmail: false,
          emailErrorMsg: "",
          disabledBtnAddMember: false
        });
      }
    } else {
      this.setState({
        errorEmail: true,
        emailErrorMsg: this.state.errorRequieredEmail,
        disabledBtnAddMember: true
      });
    }
  }

  onChangeMemberName = () => {
    if (this.memberName.current.value.trim() == "") {
      this.setState({
        errorMemberName: true
      })
    } else {
      this.setState({
        errorMemberName: false
      })
    }
    this.validateDisabledBtnAddMember();
  }

  onChangeMemberLastname = () => {
    if (this.memberLastname.current.value.trim() == "") {
      this.setState({
        errorMemberLastName: true
      })
    } else {
      this.setState({
        errorMemberLastName: false
      })
    }
    this.validateDisabledBtnAddMember();
  }

  onChangeEmail = (e) => {
    if (this.memberEmail.current.value.trim() == "") {
      this.setState({
        errorEmail: true
      })
    } else {
      this.setState({
        errorEmail: false
      })
    }
    this.validateEmail(e);

  }

  lowerCase = (e) => {
    var emailValue = this.memberEmail.current.value.toLowerCase();
    this.memberEmail.current.value = emailValue.trim();
  }

  validateDisabledBtnAddMember = () => {
    if (this.memberName.current.value.trim() === "" || this.memberLastname.current.value.trim() === "" || this.memberEmail.current.value.trim() === ""){
      this.setState({
        disabledBtnAddMember: true
      })
    } else {
      this.setState({
        disabledBtnAddMember: false
      })
    }
  }

  addMeOnGroup = () =>{
    let mySelf = {
      firstName: this.state.loggedUser.givenName,
      lastName: this.state.loggedUser.familyName,
      email: this.state.loggedUser.email,
    }
    const newMembers = [...this.state.membersList, mySelf]
    const newAdminList = [...this.state.adminList, mySelf]
    this.setState({
      membersList: newMembers,
      adminList: newAdminList
    });
  }

  resetFiedls = (e) => {
    e.preventDefault()
    var resetArr = [];
    if(this.groupName.current != null){
      this.groupName.current.value = "";
    }
    if(this.memberName.current != null){
      this.memberName.current.value = "";
    }
    if(this.memberLastname.current != null){
      this.memberLastname.current.value = "";
    }
    if(this.memberEmail.current != null){
      this.memberEmail.current.value = "";
    }
    this.setState({
      membersList: resetArr,
      disabledBtnAddMember: true,
      disabledBtn: false,
      showMembersTable : false,
    });
    this.toggleMemberForm(false);
    this.back();
  }

  onClickaddMember = () =>{
    this.toggleMemberForm(true);
    this.setState({
      showMembersTable : true,
    });
  }

  alreadyExistsMember = (newMember) => {
    var arrMembers = this.state.membersList;
    var alreadyExists = arrMembers.find(member => member.email == newMember.email);
    if(alreadyExists){
      return true;
    } else {
      return false;
    }
  }

  showSuccessToast = (message, showGroupList) => {
    this.setState({
      showToast: true,
      toastMessage: message,
      toastStyle: "modal-create-group-success",
      toastIcon : faCheckCircle
    });
    setTimeout(function() { //Start the timer
      this.setState({showToast : false}) //After 1 second, set showModal to false
      if(showGroupList){
        this.showGroupList();
      }
    }.bind(this), 2000)
  }

  showErrorToast = (message) => {
    this.setState({
      showToast: true,
      toastMessage: message,
      toastStyle: "modal-create-group-error",
      toastIcon : faExclamationCircle
    });
    setTimeout(function() { //Start the timer
      this.setState({showToast : false}) //After 1 second, set showModal to false
    }.bind(this), 2000)
  }

  showGroupList= () =>{
    this.state.globalHistory.push({
       pathname: '/groups',
    });
 }

 back = () => {
  let pathReturn =  '/groups/';
  this.state.globalHistory.push({
      pathname: pathReturn
   })
}

deleteMember= (member, warningFlag) =>{
  
  if(member.email == this.state.loggedUser.email){
    this.showWarningDialog(member, this.state.warningMemberMessage)
  } else {
    if(this.state.membersList.length > 1 && member.email != this.state.loggedUser.email){
      var newArrMembers = this.removeItemFromArr(member);
      this.setState({
        membersList: newArrMembers
      })
      this.showSuccessToast(this.state.memberDeleteSuccessMessage);
    } else {
      if(this.state.membersList.length == 1){
        this.showErrorToast(this.state.memberDeleteErrorMessage);
      }
    }
  }
 
  
}

removeItemFromArr = ( memberToDelete )=> {
  var arrMemberList = this.state.membersList;
  return arrMemberList.filter( function( member ) {
      return member.email !== memberToDelete.email;
  } );
};


showWarningDialog = (member,message) => {
  this.setState({
    showDialog: true,
    dialogMessage: message,
    dialogStyle: "modal-warning-member-delete",
    dialogMemberDelete: member
  });
}

closeDialog() {
  this.setState({
    showDialog : false
  })
}

confirmDialogDeleteMember = () => {
  if(this.state.membersList.length > 1 && this.state.dialogMemberDelete.email == this.state.loggedUser.email){
    var newArrMembers = this.removeItemFromArr(this.state.dialogMemberDelete);
    this.setState({
      membersList: newArrMembers
    })
    this.showSuccessToast(this.state.memberDeleteSuccessMessage);
  } else {
    this.showErrorToast(this.state.memberDeleteErrorMessage);
  }
  this.closeDialog();
}
  

  render() {
    return (
      <React.Fragment>
       
        <ToastDemo show={this.state.showToast} message={this.state.toastMessage} background={this.state.toastStyle} icon={this.state.toastIcon}/>

        <Modal
          size="sm"
          animation={true}
          show={this.state.showDialog}
          aria-labelledby="example-modal-sizes-title-sm"
          contentClassName={this.state.dialogStyle}
          onHide={() => this.setState({
            showDialog : false
          })}
          aria-labelledby="example-modal-sizes-title-sm"
        >
          
          <Modal.Header closeButton>
            <Modal.Title>Cuidado</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <p>{this.state.dialogMessage}</p>
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary"onClick={() => this.closeDialog()}>Cerrar</Button>
            <Button variant="primary" onClick={() => this.confirmDialogDeleteMember()}>Eliminar</Button>
          </Modal.Footer>
      </Modal>



        <div className="container col-sm-12 col-lg-6">
          { !this.state.loader && 
          <div className="jumbotron">
            <div className = "cursorPointer" style = {{marginBottom:"24px"}}onClick={() => this.back()}>
                <FontAwesomeIcon icon={faArrowLeft} />
            </div>
          <div>
            <label className="titlePage">Nuevo Grupo</label>
          </div>
            <div>
              <form onSubmit={this.submitForm}>

                <div style={{ marginTop: "24px", width: "100%" }}>
                  <div className="col-sm-12 col-md-12 col-lg-12">
                    <label className="form-label">Nombre de Grupo</label>
                    <input onChange={this.onChangeGroupName} ref={this.groupName} type="text" className="form-control" id="firstName" placeholder="" required />
                  </div>
                  {
                    this.state.showMembersTable &&
                    <div className="col-sm-12 col-lg-12">
                      <div style={{ marginTop: "16px" }}>
                        <label className="titleClass">
                          Integrantes
                        </label>
                      </div>
                      <div className="table-responsive">
                        <table className="table table-striped table-sm">
                          <thead>
                            <tr>
                              <th>Nombre</th>
                              <th>Apellido</th>
                              <th>Email</th>
                              <th>Acción</th>
                            </tr>
                          </thead>
                          <tbody>
                            {Object.entries(this.state.membersList).map(([key, member]) => (
                              <tr key={key}>
                                <td>{member.firstName}</td>
                                <td>{member.lastName}</td>
                                <td>{member.email}</td>
                                <td><FontAwesomeIcon icon={faTrash} onClick={() => this.deleteMember(member)} /></td>
                              </tr>
                            ))}

                          </tbody>
                        </table>
                      </div>
                    </div>
                  }

                  {
                    this.state.showBtn &&
                    <div style={{ width: "100%", marginBottom: "120px" }}>
                      <div className="btnRight col-sm-12 col-md-12 col-lg-12" style={{ marginTop: "24px" }}>
                        <button className="btn btn-primary btn-sm" onClick={()=>this.onClickaddMember()}>
                          <FontAwesomeIcon icon={faUserPlus} />
                          <span> Agregar integrante</span>
                        </button>
                      </div>
                    </div>
                  }
                  {
                    this.state.showMemberForm &&
                    <div className="card" style={{ marginTop: "24px", width: "100%", marginLeft: "16px", backgroundColor: "#fafafa" }}>
                      <div className="card-header">
                        <h4>Ingrese un miembro del grupo</h4>
                      </div>
                      <form>
                        <div style={{ marginTop: "24px", marginBottom: "24px", width: "100%" }}>
                          <div className="col-sm-12 col-md-10 col-lg-10">
                            <label className="form-label" style={{ marginTop: "16px" }}>Nombre</label>
                            <input onChange={this.onChangeMemberName} ref={this.memberName} type="text" required className="form-control" id="lastName" placeholder="" required />
                            {this.state.errorMemberName &&
                              <div>
                                <label className="errorLabel">
                                  *Nombre requerido.
                          </label>
                              </div>
                            }
                            <label className="form-label" style={{ marginTop: "16px" }}>Apellido</label>
                            <input onChange={this.onChangeMemberLastname} ref={this.memberLastname} type="text" className="form-control" id="email" placeholder="" required />
                            {this.state.errorMemberLastName &&
                              <div>
                                <label className="errorLabel">
                                  *Apellido requerido.
                          </label>
                              </div>
                            }
                            <label className="form-label" style={{ marginTop: "16px" }}>Email</label>
                            <input onChange={this.onChangeEmail} onKeyUp={this.lowerCase} ref={this.memberEmail} type="text" className="form-control" id="firstName" placeholder="" required autoCompleteType="email" />
                            {this.state.errorEmail &&
                              <label className="errorLabel">
                                {this.state.emailErrorMsg}
                              </label>
                            }
                          </div>

                          <div>
                            <div className="col-sm-12 col-md-10 col-lg-10 btnsContainer offset-1 " >
                              <button className="btn btn-success btn-md buttonSize" onClick={this.addMember} 
                              disabled={this.state.disabledBtnAddMember}>Agregar</button>
                              <button className="btn btn-outline-secondary btn-md buttonSize" style={{ marginLeft: "24px" }} onClick={()=>this.toggleMemberForm(false)}>Cancelar</button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  }
                  <div>
                    <div className="col-sm-12 col-md-12 col-lg-12 btnsContainer offset-1" style={{ marginBottom: "24px" }}>



                      <button className="btn btn-primary btn-md buttonSize" type="submit" disabled={this.state.disabledBtn} >Guardar</button>

                      <button className="btn btn-secondary btn-md buttonSize" type="button" style={{ marginLeft: "24px" }} onClick={this.resetFiedls}>Cancelar</button>
                    </div>

                  </div>

                </div>
              </form>
            </div>
          </div>
          }
           { this.state.loader &&
            <div>
                  <Spinner/>
            </div>
          } 
          
        </div>
        
      </React.Fragment>
    );
  }

}

export default FormGroup;