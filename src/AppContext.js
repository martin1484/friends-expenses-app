import React, {createContext, useState} from 'react';

export const AppContext = createContext();

export const AppProvider = props => {
    const [isLogged, setIsLogged] = useState(false);
    return (  
        <AppContext.Provider value={{ sessionState: [isLogged, setIsLogged] }}>
            {props.children}
        </AppContext.Provider>
    );
}