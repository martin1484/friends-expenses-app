
export const currencyFormatter = new Intl.NumberFormat('es-AR', {
    style: 'currency',
    currency: 'ARS',
    minimumFractionDigits: 2
})

export const formatDate = (dateStr, type) => {
        var formatedDate = "";
        if(dateStr){
            if(type == "short"){
                const date3 = new Date(dateStr);
                var meses = new Array ("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");
                formatedDate = date3.getDate()+" de "+ meses[date3.getMonth()];
                return formatedDate
            } else {
                const date3 = new Date(dateStr);
                var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
                formatedDate = date3.getDate()+" de "+ meses[date3.getMonth()] + " del " + date3.getFullYear();
                return formatedDate
            }
        }
}

export const elipsisData = function(data, maxLength){
    return data.substr(0, maxLength) + "...";
};